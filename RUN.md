# How To Run App

-   clone repo

```
git clone git@gitlab.com:khaled-zubaidi/semsari.git
```

# Using Docker

```
cp .env.example .env
```

-   set app .env file for local environment

```
#for Docker only
SAIL_APP_PORT='81'#OR '81' or '8000' OR '88' or '8080'
SAIL_VITE_PORT='5173' #or 8000 or 7000
SAIL_FORWARD_DB_PORT='5433' # 54320 or 5433 or 5454
APP_SERVICE='sumaya369.dev' #most be same name as services name
SAIL_FORWARD_REDIS_PORT='6378'#or 6380
SAIL_FORWARD_MAILHOG_PORT='1024' #or any number
SAIL_FORWARD_MAILHOG_DASHBOARD_PORT='8026' #or any number
```

-   remove composer.lock

```
rm composer.lock
```

-   run docker

```
docker-compose up --build

```

-   run artisan

```
docker exec -it api.semsari.dev /bin/bash
```

-   generat key - optimize

```
php artisan key:generat
php artisan optimize:clear
php artisan ide-helper:generate
php artisan ide-helper:models -n
```

-   app links
    -   main web : http://localhost:8011/
    -   nova : http://localhost:8011/nova
    -   log-viewer : http://localhost:8011/log-viewer
    -   log-viewer : http://localhost:8011/log-viewer
-   other links

    -

-   run links http://localhost:8000/ or http://localhost:YourPort/
    -   laravel nova will be in http://localhost:8000/nova
        -   <p align="center"><a href="http://localhost:8000/nova" target="_blank"><img src="public/images/readme/nova-login-screen.png" width="250"></a></p>
        -   login user admin@admin.com
        -   login password 123456789
        -   you should run
        ```
        php artisan migrate
        php artisan db:seed
        #or
        php artisan migrate:refresh --force --seed
        #or for faset drop database.
        php artisan migrate:fresh --force --seed
        ```
    -   Log-Viewer http://localhost:8000/log-viewer
        -   No need to login in local environments.
        -   You have to login using nova in non-local environments.
-   Before start coding

    -   Update local helper files
        ```
        composer run ide-helper
        ```

-   After adding new code run

    -   Fix style in php code

        ```
        ./vendor/bin/pint
        ```


-   Run Test

```
touch -> database/database.sqlite
cp .env.testing.example .env.testing
php artisan optimize --env=testing
./vendor/bin/pest --stop-on-failure  --parallel
```

-   Run Envoy {{only for testing use}}
    -   you must register your public key in server

```
composer global require laravel/envoy
php vendor/bin/envoy run deploy --branch=testing
```
