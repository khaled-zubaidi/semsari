<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reviews', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('rated_by');
            $table->unsignedBigInteger('rated_user');
            $table->string('comment');
            $table->integer('rate');
            $table->timestamps();
        });


        Schema::table('reviews', function (Blueprint $table) {


            $table->foreign('rated_user')
                ->references('id')
                ->on('users');

            $table->foreign('rated_by')
                ->references('id')
                ->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reviews');
    }
};
