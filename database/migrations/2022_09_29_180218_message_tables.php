<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messages', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('mailbox_id');
            $table->unsignedBigInteger('sender_id');
            $table->string('message');
            $table->timestamps();
        });

        Schema::create('mailboxes', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('recipient_id');
            $table->timestamps();
        });

        Schema::table('messages', function (Blueprint $table) {

            $table->foreign('mailbox_id')
                ->references('id')
                ->on('mailboxes');

            $table->foreign('sender_id')
                ->references('id')
                ->on('users');
        });

        Schema::table('mailboxes', function (Blueprint $table) {

            $table->foreign('user_id')
                ->references('id')
                ->on('users');

            $table->foreign('recipient_id')
                ->references('id')
                ->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messages');
        Schema::dropIfExists('mailboxes');
    }
};
