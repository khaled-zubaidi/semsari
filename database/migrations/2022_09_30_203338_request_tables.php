<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requests', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('requester_id');
            $table->string('transaction_type');
            $table->string('property_type');
            $table->date('rent_from');
            $table->date('rent_to');
            $table->string('location');
            $table->integer('price_from');
            $table->integer('price_to');
            $table->string('specification');
            $table->double('area', 6, 2);
            $table->integer('members');
            $table->string('special_request');
            $table->string('status');
            $table->timestamps();

            $table->foreign('requester_id')
                ->references('id')
                ->on('users');
        });

        Schema::create('broker_requests', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('broker_id');
            $table->unsignedBigInteger('request_id');
            $table->string('status');
            $table->timestamps();

            $table->foreign('broker_id')
                ->references('id')
                ->on('users');

            $table->foreign('request_id')
                ->references('id')
                ->on('requests');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requests');
        Schema::dropIfExists('broker_requests');
    }
};
