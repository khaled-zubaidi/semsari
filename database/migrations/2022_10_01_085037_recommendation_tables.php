<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recommendations', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('requester_id');
            $table->unsignedBigInteger('broker_id');
            $table->string('transaction_type');
            $table->string('property_type');
            $table->string('location');
            $table->integer('price');
            $table->string('specification');
            $table->double('area', 6, 2);
            $table->integer('members');
            $table->string('chosen_slot');
            $table->string('availability');
            $table->string('status');
            $table->timestamps();

            $table->foreign('requester_id')
                ->references('id')
                ->on('requests');

            $table->foreign('broker_id')
                ->references('id')
                ->on('users');
        });

        Schema::create('recommendation_media', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('recommendation_id');
            $table->string('content');
            $table->string('type');
            $table->timestamps();

            $table->foreign('recommendation_id')
                ->references('id')
                ->on('recommendations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recommendations');
        Schema::dropIfExists('recommendation_media');
    }
};
