<?php

namespace App\Services\Notifications;

use App\Models\User;
use App\Services\Notifications\Drivers\EmailDriver;

class EmailNotificationService
{

    protected EmailDriver $driver;

    public function __construct(
        EmailDriver $driver)
    {

        $this->driver = $driver;
    }

    public function sendAnonymous($subject, $to, $template, $data){

        $this->driver->execute(
            $subject,
            $to,
            $template,
            $data);
    }

    public function send($subject, User $user, $template, $data){

        $this->driver->execute(
            $subject,
            $user->email,
            $template,
            $data);
    }
}
