<?php

namespace App\Services\Notifications\Drivers;

use Mail;

class EmailDriver
{

    public function execute(
        $subject,
        $to,
        $template,
        array $data){

        Mail::send($template, ['data' => $data], function ($m) use ($to, $subject) {
            $m->from('hello@app.com', 'Your Application');

            $m->to($to)->subject($subject);
        });
    }
}
