<?php

namespace App\Services\Notifications\Drivers;

use GuzzleHttp\Client;

class SmsDriver
{

    public function execute($to, $message){

        $client = new Client([
            'base_uri' => env("SMS_VENDOR_HOST"),
            'headers' => [
                'Authorization' => 'Bearer ' . env("SMS_VENDOR_SECRET")
            ],
            'query' => [
                'sender' => env("SMS_VENDOR_SENDER_NAME"),
                'recipients' => $to,
                'body' => $message
            ]
        ]);
        $client->request('POST', env("SMS_VENDOR_PATH"));
    }
}
