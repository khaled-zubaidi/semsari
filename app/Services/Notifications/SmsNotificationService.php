<?php

namespace App\Services\Notifications;

use App\Services\Notifications\Drivers\SmsDriver;

class SmsNotificationService
{

    protected SmsDriver $driver;

    public function __construct(
        SmsDriver $driver)
    {

        $this->driver = $driver;
    }

    public function send(){

        $this->driver->execute();
    }
}
