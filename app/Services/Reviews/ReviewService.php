<?php

namespace App\Services\Reviews;

use App\Exceptions\UserNotFoundException;
use App\Models\Reviews\Review;
use App\Models\User;
use Auth;
use DB;

class ReviewService
{

    public function review(
        $userId,
        $rate,
        $comment): void
    {

        $user = User::find($userId);
        if(!$user || $user->role === 'BUYER'){

            throw new UserNotFoundException();
        }

        Review::create([
            'comment' => $comment,
            'rated_user' => $userId,
            'rated_by' => Auth::user()->id,
            'rate' => $rate
        ]);
    }

    public function list($userId){

        $user = User::find($userId);
        if(!$user || $user->role === 'BUYER'){

            throw new UserNotFoundException();
        }

        return Review::where(['rated_user' => $userId])
            ->get();
    }

    public function avgRating($brokerId)
    {

        return DB::table('reviews')
            ->where('rated_user', $brokerId)
            ->avg('rate');
    }
}
