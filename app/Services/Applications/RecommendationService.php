<?php

namespace App\Services\Applications;

use App\Exceptions\BrokerNotAssigendToRequestException;
use App\Exceptions\ChosenSlotNotAvailableException;
use App\Exceptions\RecommendationNotFoundException;
use App\Exceptions\RequestNotFoundException;
use App\Models\Applications\BrokerRequest;
use App\Models\Applications\Recommendation;
use App\Models\Applications\RecommendationMedia;
use App\Models\Applications\Request;
use App\Services\Applications\Enum\BrokerRequestStatus;
use App\Services\Applications\Enum\RecommendationStatus;

class RecommendationService
{

    public function create(array $data){

        $request = Request::find($data['request_id']);
        if(!$request){

            throw new RequestNotFoundException();
        }

        $isBrokerAssigned = BrokerRequest::where([
            'broker_id' => \Auth::user()->id,
            'request_id' => $request->id,
            'status' => BrokerRequestStatus::ACCEPTED,
        ])->first();
        if(!$isBrokerAssigned){

            throw new BrokerNotAssigendToRequestException();
        }

        $recommendation = Recommendation::create([
            'requester_id' => $request->id,
            'broker_id' => \Auth::user()->id,
            'transaction_type' => $data['transaction_type'],
            'property_type' => $data['property_type'],
            'location' => $data['location'],
            'price' => $data['price'],
            'specification' => join('#', ((array) $data['specification'])),
            'area' => $data['area'],
            'members' => $data['members'],
            'status' => RecommendationStatus::NEW,
            'chosen_slot' => "",
            'availability'  => join('#', ((array) $data['availability']))
        ]);

        foreach ($data['media'] as &$value) {

            RecommendationMedia::create([
                'recommendation_id' => $recommendation->id,
                'content' => $value['content'],
                'type' => $value['type'],
            ]);
        }
    }

    public function chooseSlot($recommendationId, $slot){

        $recommendation = Recommendation::find($recommendationId);
        if(!$recommendation){

            throw new RecommendationNotFoundException();
        }

        $isSlotAvailable = str_contains($recommendation->availability, $slot);
        if(!$isSlotAvailable){

            throw new ChosenSlotNotAvailableException();
        }

        $request = Request::where([
            'id' => $recommendation->request_id,
            'requester_id' => \Auth::user()->id,
        ]);
        if(!$request){

            throw new RequestNotFoundException();
        }

        $recommendation->chosen_slot = $slot;
        $recommendation->status = RecommendationStatus::ACCEPTED;
        $recommendation->save();
    }

    public function fetch($requestId, $brokerId)
    {

        $request = Request::find($requestId);
        if(!$request){

            throw new RequestNotFoundException();
        }

        $recommendation = Recommendation::where([
            'requester_id' => $requestId,
            'broker_id' => $brokerId,
        ])->get();

        foreach($recommendation as &$recommend){
            $recommend->match_rate = $this->calcMatch(
                $recommend,
                $request,
            );
        }

        return ['recommendations' => $recommendation];
    }

    public function fetchById($recommendationId)
    {

        $recommendation =  Recommendation::find($recommendationId);
        if(!$recommendation){

            throw new RecommendationNotFoundException();
        }

        $request = Request::find($recommendation->request_id);
        if(!$request){

            throw new RequestNotFoundException();
        }

        $recommendation->match_rate = $this->calcMatch(
            $recommendation,
            $request,
        );

        return ['recommendations' => $recommendation];
    }

    private function calcMatch($recommend, $request)
    {
        $points = 0;

        if($recommend->transaction_type === $request->transaction_type){
            $points += 1;
        }

        if($recommend->property_type === $request->property_type){
            $points += 1;
        }

        if($recommend->location === $request->location){
            $points += 1;
        }

        if($recommend->area === $request->area){
            $points += 1;
        }

        if($recommend->members === $request->members){
            $points += 1;
        }

        $recommendSpecification = explode('#', $recommend->specification);
        $eqSpecification = explode('#', $request->specification);

        foreach($recommendSpecification as $spe){

            if(in_array($spe, $eqSpecification))
                $points += 1;
        }

        return ($points / (5 + count($eqSpecification))) * 100;
    }
}
