<?php

namespace App\Services\Applications\Enum;

enum BrokerRequestStatus: string
{
    case ACCEPTED = 'ACCEPTED';
    case REJECTED = 'REJECTED';
    case PENDING = 'PENDING';
}
