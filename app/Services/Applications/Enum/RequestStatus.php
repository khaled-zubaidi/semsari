<?php

namespace App\Services\Applications\Enum;

enum RequestStatus: string
{
    case NEW = 'NEW';
    case CLOSED = 'CLOSED';
}
