<?php

namespace App\Services\Applications\Enum;

enum TransactionType: string
{
    case RENT = 'RENT';
    case BUY = 'BUY';
}
