<?php

namespace App\Services\Applications\Enum;

enum RequestSpecification: string
{
    case LIVING_ROOM = 'LIVING_ROOM';
    case BATHTUB = 'BATHTUB';
    case POOL = 'POOL';
    case PARKING = 'PARKING';
    case KITCHEN = 'KITCHEN';
    case GARDEN = 'GARDEN';
    case AC = 'AC';
    case TV_ROOM = 'TV_ROOM';
    case WIFI = 'WIFI';
}
