<?php

namespace App\Services\Applications\Enum;

enum RecommendationStatus: string
{
    case NEW = 'NEW';
    case ACCEPTED = 'ACCEPTED';
}
