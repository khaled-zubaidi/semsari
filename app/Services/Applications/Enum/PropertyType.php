<?php

namespace App\Services\Applications\Enum;

enum PropertyType: string
{
    case VILLA = 'VILLA';
    case APARTMENT = 'APARTMENT';
    case LAND = 'LAND';
}
