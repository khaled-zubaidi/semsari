<?php

namespace App\Services\Applications;

use App\Exceptions\BrokerCannotBeCloseApplication;
use App\Exceptions\BrokerCannotBeReviewer;
use App\Exceptions\BuyerCannotApprovedRequests;
use App\Exceptions\BuyerDoseNotHaveAssignedRequestsException;
use App\Exceptions\RequestBrokerNotFoundException;
use App\Exceptions\RequestNotFoundException;
use App\Models\Applications\BrokerRequest;
use App\Models\Applications\Request;
use App\Models\Profiles\Profile;
use App\Models\User;
use App\Services\Applications\Enum\BrokerRequestStatus;
use App\Services\Applications\Enum\PropertyType;
use App\Services\Applications\Enum\RequestStatus;
use App\Services\Applications\Enum\TransactionType;
use Auth;

class ApplicationService
{

    public function create(
        array $data){

        $request = Request::create([
            'requester_id' => Auth::user()->id,
            'transaction_type' => TransactionType::from($data['transaction_type']),
            'property_type' => PropertyType::from($data['property_type']),
            'rent_from' => $data['rent_from'],
            'rent_to' => $data['rent_to'],
            'location' => $data['location'],
            'price_from' => $data['price_from'],
            'price_to' => $data['price_to'],
            'specification' => join('#', ((array)$data['specification'])),
            'area' => $data['area'],
            'members' => $data['members'],
            'special_request' => $data['special_request'],
            'status' => RequestStatus::NEW,
        ]);

        foreach ($data['brokers'] as &$value) {

            $broker = BrokerRequest::create([
                'broker_id' => $value,
                'request_id' => $request->id,
                'status' => BrokerRequestStatus::PENDING,
            ]);
        }
    }

    public function fetch(){

        $requests = Request::where([
            'requester_id' => Auth::user()->id])
            ->get();

        return ['requests' => $requests];
    }

    public function fetchById($requestId){

        $user = Auth::user();
        $request = Request::find($requestId);

        if($user->role === 'BROKER'){

            $request->broker_status = BrokerRequest::where([
                'broker_id' => $user->id,
                'request_id' => $requestId
                ])
                ->first()
                ->status;
        }

        return $request;
    }

    public function assigned(){

        $user = Auth::user();
        if($user->role === 'BUYER'){

            throw new BuyerDoseNotHaveAssignedRequestsException();
        }

        $requestIds = BrokerRequest::where([
            'broker_id' => $user->id,
        ])->where('status', '!=' , BrokerRequestStatus::REJECTED)
            ->
        get()->pluck('request_id');

        $requests = Request::whereIn('id', $requestIds)
            ->get();

        return ['requests' => $requests ];
    }

    public function broekrsByRequestId($requestId){

        $user = Auth::user();
        if($user->role === 'BROKER'){

            throw new BuyerDoseNotHaveAssignedRequestsException();
        }

        $requestIds = BrokerRequest::where([
            'request_id' => $requestId
        ])->get()->pluck('broker_id');

        $brokers = Profile::whereIn('user_id', $requestIds)
            ->get();

        foreach ($brokers as &$br){

            $br->name = User::find($br->user_id)->name;
            $broker = BrokerRequest::where([
                'broker_id' => $br->user_id,
                'request_id' => $requestId
            ])->first();

            if(!$broker) {
                $br->broker_status = '';
            } else {
                $br->broker_status =  $broker->status;
            }
        }
        return ['brokers' => $brokers];
    }

    public function approve($requestId){

        $user = Auth()->user();
        if($user->role === 'BUYER'){

            throw new BuyerCannotApprovedRequests();
        }

        $request = Request::find($requestId);
        if(!$request){

            throw new RequestNotFoundException();
        }

        $requestBroker = BrokerRequest::where([
            'broker_id' => $user->id,
            'request_id' => $request->id,
            'status' => BrokerRequestStatus::PENDING,
        ])->first();
        if(!$requestBroker){

            throw new RequestBrokerNotFoundException();
        }

        $requestBroker->status = BrokerRequestStatus::ACCEPTED;
        $requestBroker->save();


        return $request;
    }

    public function reject($requestId){

        $user = Auth()->user();
        if($user->role === 'BUYER'){

            throw new BuyerCannotApprovedRequests();
        }

        $request = Request::find($requestId);
        if(!$request){

            throw new RequestNotFoundException();
        }

        $requestBroker = BrokerRequest::where([
            'broker_id' => $user->id,
            'request_id' => $request->id,
            'status' => BrokerRequestStatus::PENDING,
        ])->first();
        if(!$requestBroker){

            throw new RequestBrokerNotFoundException();
        }

        $requestBroker->status = BrokerRequestStatus::REJECTED;
        $requestBroker->save();
    }

    public function reviewable($requestId){

        $user = Auth()->user();
        if($user->role === 'BROKER'){

            throw new BrokerCannotBeReviewer();
        }

        $request = Request::where([
            'id' => $requestId,
            'requester_id' => $user->id,
            'status' => RequestStatus::CLOSED
        ]);
        if(!$request){

            throw new RequestNotFoundException();
        }

        return $request;
    }

    public function close($requestId){

        $user = Auth()->user();
        if($user->role === 'BROKER'){

            throw new BrokerCannotBeCloseApplication();
        }

        $request = Request::where([
            'id' => $requestId,
            'requester_id' => $user->id,
            'status' => RequestStatus::NEW
        ])->first();
        if(!$request){

            throw new RequestNotFoundException();
        }

        $request->status = RequestStatus::CLOSED;
        $request->save();

        return $request;
    }

    public function messagable($requestId)
    {

        $user = Auth()->user();
        $request = Request::where([
            'id' => $requestId,
            'requester_id' => $user->id,
            'status' => RequestStatus::NEW
        ]);
        if(!$request){

            throw new RequestNotFoundException();
        }
    }
}
