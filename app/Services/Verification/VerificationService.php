<?php

namespace App\Services\Verification;

use App\Exceptions\VerificationIncorrectException;
use App\Models\Verifications\Verification;
use App\Services\Verification\Enums\VerifyThrough;
use Illuminate\Support\Str;
use VerificationThrough;

class VerificationService
{

    public function generate(
        VerifyThrough $verificationThrough){

        $identifier = Str::uuid();

        Verification::create([
            'identifier' => $identifier,
            'token' => random_int(100000, 999999),
            'is_used' => false,
            'verify_through' => $verificationThrough
        ]);

        return [
            'identifier' => $identifier
        ];
    }

    public function verify(
        $identifier,
        $token,
        VerifyThrough $verificationThrough){

        $verification = Verification::where([
            'identifier' => $identifier,
            'token' => $token,
            'is_used' => false,
            'verify_through' => $verificationThrough])
            ->first();

        if(!$verification)
            throw new VerificationIncorrectException();

        $verification->is_used = true;
        $verification->save();
    }
}
