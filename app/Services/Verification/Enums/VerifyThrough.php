<?php

namespace App\Services\Verification\Enums;

abstract class VerifyThrough
{
    const SYSTEM = 'SYSTEM';
    const API = 'API';
}
