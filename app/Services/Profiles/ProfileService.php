<?php

namespace App\Services\Profiles;

use App\Exceptions\ProfileNotFoundException;
use App\Exceptions\UserProfileNotAllowed;
use App\Models\Profiles\Profile;
use App\Models\User;
use Auth;
use DB;

class ProfileService
{
    public function create(
        array $data) {

        $user = Auth::user();

        if($user->role === 'BUYER'){

            throw new UserProfileNotAllowed();
        }

        Profile::create([
            'user_id' => $user->id,
            'gender' => $data['gender'],
            'location' => $data['location'],
            'years_of_exp' => $data['years_of_exp'],
            'bio' => $data['bio'],
            'property_type' => join('#', (array)$data['property_type']),
            'transaction_type' => join('#', (array)$data['transaction_type']),
            'service_fees' => $data['service_fees'],
            'commission_rate' => $data['commission_rate'],
        ]);
    }

    public function update(
        array $data) {

        $user = Auth::user();
        $profile = Profile::where(['user_id' => $user->id])
            ->first();

        if($user->role === 'BUYER'){

            throw new UserProfileNotAllowed();
        }

        if(!$profile) {

            throw new ProfileNotFoundException();
        }

        $profile->update([
            'gender' => $data['gender'],
            'location' => $data['location'],
            'years_of_exp' => $data['years_of_exp'],
            'bio' => $data['bio'],
            'property_type' => join('#', (array)$data['property_type']),
            'transaction_type' => join('#', (array)$data['transaction_type']),
            'service_fees' => $data['service_fees'],
            'commission_rate' => $data['commission_rate'],
        ]);
    }

    public function search(array $data){

         return DB::table('profiles')
            ->select('*')
            ->where('property_type', 'like', '%'.$data['property_type'].'%')
            ->where('transaction_type', 'like', '%'.$data['transaction_type'].'%')
            ->where('location', 'like', '%'.$data['location'].'%')
             ->join('users', 'users.id', '=','profiles.user_id')
            ->orderBy('service_fees', 'desc')
            ->get();
    }

    public function fetch() {

        $user = Auth::user();

        if($user->role === 'BUYER'){

            return [
                'name' => $user->name,
                'email' => $user->email,
            ];
        }


        $profile = Profile::where(['user_id' => $user->id])
            ->first();

        if(!$profile) {

            throw new ProfileNotFoundException();
        }

        $profile->transaction_type = explode('#', $profile->transaction_type);
        $profile->property_type = explode('#', $profile->property_type);

        return [
            'name' => $user->name,
            'email' => $user->email,
            'profile' => $profile,
        ];
    }

    public function fetchById($brokerId)
    {
        $user = User::find($brokerId);

        if($user->role === 'BUYER'){

            return [
                'name' => $user->name,
                'email' => $user->email,
            ];
        }

        $profile = Profile::where(['user_id' => $user->id])
            ->first();

        if(!$profile) {

            throw new ProfileNotFoundException();
        }

        $profile->transaction_type = explode('#', $profile->transaction_type);
        $profile->property_type = explode('#', $profile->property_type);

        return [
            'name' => $user->name,
            'email' => $user->email,
            'profile' => $profile,
        ];
    }
}
