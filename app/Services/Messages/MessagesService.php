<?php

namespace App\Services\Messages;

use App\Exceptions\MailboxNotFoundException;
use App\Exceptions\UserNotFoundException;
use App\Models\Mailbox;
use App\Models\Messages\Message;
use App\Models\User;
use Auth;

class MessagesService
{

    public function list(){

        $user = Auth::user();

        $mailbox = Mailbox::where([
            'user_id' => $user->id,
            ])->orWhere([
                'recipient_id' => $user->id
        ])->get();

        foreach ($mailbox as &$m){

            $lastMessage = Message::where([
                'mailbox_id' => $m->id
            ])->first();
            $m->last_message_from = User::find($lastMessage->sender_id)->name;
            $m->last_message = $lastMessage->message;
        }

        return ['messages' => $mailbox];
    }

    public function fetch($mailboxId){

        $user = Auth::user();
        $mailbox = Mailbox::where([
            'user_id' => $user->id,
            'id' => $mailboxId
        ])->orWhere([
            'recipient_id' => $user->id,
            'id' => $mailboxId
        ])->first();

        if(!$mailbox){

            throw new MailboxNotFoundException();
        }

        $messages = $mailbox->getMessages()
            ->get();

        return ['messages' => $messages];
    }

    public function send($recipientId, $message){

        $user = Auth::user();
        $recipient = User::find($recipientId);

        if(!$recipient){

            throw new UserNotFoundException();
        }

        $mailbox = Mailbox::where([
            'user_id' => $user->id,
            'recipient_id' => $recipientId,
            ])->orWhere([
            'user_id' => $recipientId,
            'recipient_id' => $user->id,
        ])->first();

        if(!$mailbox){

            $mailbox = Mailbox::firstOrCreate([
                'user_id' => $user->id,
                'recipient_id' => $recipientId,
            ]);
        }

        $mailbox->getMessages()
            ->create([
                'sender_id' => $user->id,
                'message' => $message
            ]);
    }
}
