<?php

namespace App\Services\Authentication;

use App\Exceptions\UserIncorrectCredentialException;
use App\Models\User;
use App\Services\Notifications\EmailNotificationService;
use Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class AuthenticationService
{

    public function __construct(
        EmailNotificationService $emailNotificationService) {

        $this->emailNotificationService = $emailNotificationService;
    }

    public function create(
        $name,
        $username,
        $password,
        $email){

        User::create([
            'name' => $name,
            'username' => $username,
            'password' => $password,
            'email' => $email
        ]);
    }

    public function loginViaUsername(
        $username,
        $password){

        $user = User::where([
            'username' => $username,
        ])->firstOrFail();

        if(!Hash::check($password, $user->password)){

            throw new UserIncorrectCredentialException();
        }

        $token = $user->createToken('auth');
        return ['token' => $token, 'role' => $user->role];
    }

    public function loginViaEmail(
        $email,
        $password){

        $user = User::where([
            'email' => $email,
        ])->firstOrFail();

        if(!Hash::check($password, $user->password)){

            throw new UserIncorrectCredentialException();
        }

        $token = $user->createToken('auth');
        return ['token' => $token, 'role' => $user->role];
    }

    public function resetPassword(
        $email){

        $user = User::where([
            'email' => $email,
        ])->firstOrFail();

        $password = Str::random(6);

        $this->emailNotificationService->send(
            "Reset Password",
            $user,
            "user.forget-password",
            ["password" => $password]
        );

        $user->password = Hash::make($password);
        $user->save();
    }

    public function forceLogin(User $user){

        $token = $user->createToken('auth');
        return ['token' => $token, 'role' => $user->role];
    }

    public function logout(){

        Auth::logout();
    }
}
