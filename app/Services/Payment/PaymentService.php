<?php

namespace App\Services\Payment;

use App\Exceptions\PaymentRecordNotFoundExecption;
use App\Models\Payment;
use App\Services\Verification\Enums\PaymentStatus;
use App\Services\Verification\Enums\PaymentType;
use Illuminate\Support\Str;

class PaymentService
{

    public function create(
        PaymentType $paymentType,
        $amount) {

        $identifier = Str::uuid();

        Payment::create([
            'amount' => $amount,
            'type' => $paymentType,
            'status' => PaymentStatus::UNPAID,
        ]);

        return [
            'identifier' => $identifier
        ];
    }

    public function markAsPaid($identifier){

        $payment = Payment::where(['identifier' => $identifier])
            ->first();

        if(!$payment)
            throw new PaymentRecordNotFoundExecption();

        $payment->status = PaymentStatus::PAID;
        $payment->save();
    }
}
