<?php

namespace App\Services\Verification\Enums;

abstract class PaymentType
{
    const CASH = 0;
    const CREDIT_DEBIT = 1;
}
