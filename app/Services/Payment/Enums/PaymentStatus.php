<?php

namespace App\Services\Verification\Enums;

abstract class PaymentStatus
{
    const PAID = 0;
    const UNPAID = 1;
}
