<?php

namespace App\Services\Registration;

use App\Exceptions\UserExistsException;
use App\Models\User;
use Hash;
use UserRole;

class RegistrationService
{

    public function register(
        $email,
        $name,
        $password,
        $role = 'BUYER' | 'BROKER'){

        $doseUserExitsWithSameEmail = User::where(['email' => $email])->exists();
        if($doseUserExitsWithSameEmail){

            throw new UserExistsException();
        }

        return User::create([
            'email' => $email,
            'name' => $name,
            'role' => $role,
            'password' => Hash::make($password)
        ]);
    }
}
