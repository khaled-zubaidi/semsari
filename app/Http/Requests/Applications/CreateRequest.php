<?php

namespace App\Http\Requests\Applications;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'transaction_type' => 'required|string',
            'property_type' => 'required|string',
            'rent_from' => 'date|nullable',
            'rent_to' => 'date|nullable',
            'location' => 'required|string',
            'price_from' => 'required|numeric',
            'price_to' => 'required|numeric',
            'specification' => 'required|array',
            'area' => 'required|numeric',
            'members' => 'required|numeric',
            'special_request' => 'required|string',
        ];
    }
}
