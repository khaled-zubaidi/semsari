<?php

namespace App\Http\Requests\Applications;

use Illuminate\Foundation\Http\FormRequest;

class RecommendationCreate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'request_id' => 'required|numeric',
            'transaction_type' => 'required|string',
            'property_type' => 'required|string',
            'location' => 'required|string',
            'specification' => 'required|array',
            'area' => 'required|numeric',
            'members' => 'required|numeric',
            'price' => 'required|numeric',
            'availability' => 'required|array',
            'media' => 'array',
        ];
    }
}
