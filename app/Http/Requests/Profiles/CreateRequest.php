<?php

namespace App\Http\Requests\Profiles;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'gender' => 'required|string',
            'location' => 'required|string',
            'years_of_exp' => 'required|numeric',
            'bio' => 'required|string',
            'property_type' => 'required|array',
            'transaction_type' => 'required|array',
            'service_fees' => 'required|numeric',
            'commission_rate' => 'required|numeric',
        ];
    }
}
