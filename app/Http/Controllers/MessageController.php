<?php

namespace App\Http\Controllers;

use App\Http\Requests\Messages\FetchRequest;
use App\Http\Requests\Messages\ListRequest;
use App\Http\Requests\Messages\SendRequest;
use App\Services\Applications\ApplicationService;
use App\Services\Messages\MessagesService;
use Illuminate\Http\Request;

class MessageController extends Controller
{
    protected MessagesService $messagesService;
    protected ApplicationService $applicationService;

    public function __construct(
        ApplicationService $applicationService,
        MessagesService $messagesService)
    {

        $this->applicationService = $applicationService;
        $this->messagesService = $messagesService;
    }

    public function list(){

        return $this->messagesService->list();
    }

    public function fetch($mailboxId){

        return $this->messagesService
            ->fetch($mailboxId);
    }

    public function send(SendRequest $request){

        $this->applicationService
            ->messagable($request->input('request_id'));

        $this->messagesService
            ->send(
                $request->input('recipientId'),
                $request->input('message'));
    }
}
