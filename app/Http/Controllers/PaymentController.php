<?php

namespace App\Http\Controllers;

use App\Http\Requests\Payments\MarkAsPaidRequest;
use App\Services\Payment\PaymentService;

class PaymentController extends Controller
{
    protected PaymentService $paymentService;

    public function __construct(
        PaymentService $paymentService){

        $this->paymentService = $paymentService;
    }

    public function markAsPaid(MarkAsPaidRequest $request){

        $this->paymentService->markAsPaid(
            $request->input("identifier"));
    }
}
