<?php

namespace App\Http\Controllers;

use App\Http\Requests\Applications\CreateRequest;
use App\Services\Applications\ApplicationService;
use App\Services\Messages\MessagesService;
use Illuminate\Http\Request;

class ApplicationController extends Controller
{

    protected ApplicationService $applicationService;
    protected MessagesService $messagesService;

    public function __construct(
        MessagesService $messagesService,
        ApplicationService $applicationService)
    {

        $this->applicationService = $applicationService;
        $this->messagesService = $messagesService;
    }

    public function create(CreateRequest $request){

        $this->applicationService
            ->create($request->all());
    }

    public function reject($requestId){

        $this->applicationService
            ->reject($requestId);
    }

    public function fetch(){

        return $this->applicationService
            ->fetch();
    }

    public function fetchById($requestId){

        return $this->applicationService
            ->fetchById($requestId);
    }


    public function assigned(){

        return $this->applicationService
            ->assigned();
    }

    public function broekrsByRequestId($requestId){

        return $this->applicationService
            ->broekrsByRequestId($requestId);
    }

    public function close($requestId){

        $this->applicationService
            ->close($requestId);
    }

    public function approve($requestId){

        $request = $this->applicationService
            ->approve($requestId);

        $this->messagesService
            ->send(
                $request->requester_id,
            "I've accepted your request");
    }
}
