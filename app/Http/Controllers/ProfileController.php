<?php

namespace App\Http\Controllers;

use App\Http\Requests\Profiles\CreateRequest;
use App\Http\Requests\Profiles\SearchRequest;
use App\Http\Requests\Profiles\UpdateRequest;
use App\Services\Profiles\ProfileService;
use App\Services\Reviews\ReviewService;

class ProfileController extends Controller
{
    protected ProfileService $profileService;
    protected ReviewService $reviewService;

    public function __construct(
        ReviewService $reviewService,
        ProfileService $profileService)
    {

        $this->reviewService = $reviewService;
        $this->profileService = $profileService;
    }

    public function create(CreateRequest $request){

        $this->profileService
            ->create($request->all());
    }

    public function update(UpdateRequest $request){

        $this->profileService
            ->update($request->all());
    }

    public function search(SearchRequest $request){

        $brokers = $this->profileService
            ->search($request->all());

        foreach ($brokers as $broker){

            $broker->email_verified_at = '';
            $broker->password = '';
            $broker->remember_token = '';

            $broker->rate = (int)$this->reviewService
                ->avgRating($broker->id);
        }

        return [
            'brokers' => $brokers
        ];
    }

    public function fetch(){

        return $this->profileService
            ->fetch();
    }

    public function fetchById($brokerId){

        $broker = $this->profileService
            ->fetchById($brokerId);

        $broker['profile']->rate = (int)$this->reviewService
            ->avgRating($broker['profile']->user_id);

        return $broker;
    }
}
