<?php

namespace App\Http\Controllers;

use App\Http\Requests\Registrations\RegisterRequest;
use App\Services\Authentication\AuthenticationService;
use App\Services\Registration\RegistrationService;

class RegistrationController extends Controller
{
    protected RegistrationService $registrationService;
    protected AuthenticationService $authenticationService;

    public function __construct(
        RegistrationService $registrationService,
        AuthenticationService $authenticationService){

        $this->registrationService = $registrationService;
        $this->authenticationService = $authenticationService;
    }

    public function register(RegisterRequest $request){

        $user = $this->registrationService
            ->register(
                $request->input('email'),
                $request->input('name'),
                $request->input('password'),
                $request->input('role'));

        return $this->authenticationService
            ->forceLogin($user);
    }
}
