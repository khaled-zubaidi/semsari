<?php

namespace App\Http\Controllers;

use App\Http\Requests\Authentications\LoginRequest;
use App\Services\Authentication\AuthenticationService;
use Illuminate\Http\Request;

class AuthenticationController extends Controller
{

    protected AuthenticationService $authenticationService;

    public function __construct(
        AuthenticationService $authenticationService)
    {
        $this->authenticationService = $authenticationService;
    }

    public function login(LoginRequest $request){

        return $this->authenticationService
            ->loginViaEmail(
                $request->input('email'),
                $request->input('password'));
    }

    public function logout(){

        $this->authenticationService
            ->logout();
    }
}
