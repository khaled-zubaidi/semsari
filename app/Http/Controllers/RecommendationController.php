<?php

namespace App\Http\Controllers;

use App\Http\Requests\Applications\RecommendationChoose;
use App\Http\Requests\Applications\RecommendationCreate;
use App\Services\Applications\ApplicationService;
use App\Services\Applications\RecommendationService;
use App\Services\Messages\MessagesService;

class RecommendationController extends Controller
{
    protected RecommendationService $recommendationService;
    protected ApplicationService $applicationService;
    protected MessagesService $messagesService;

    public function __construct(
        ApplicationService $applicationService,
        RecommendationService $recommendationService,
        MessagesService $messagesService)
    {

        $this->applicationService = $applicationService;
        $this->recommendationService = $recommendationService;
        $this->messagesService = $messagesService;
    }

    public function create(RecommendationCreate $request){

        $this->recommendationService
            ->create($request->all());

        $req = $this->applicationService
            ->fetchById($request->input('request_id'));

        $this->messagesService
            ->send($req->requester_id, "I've submitted a recommendation");
    }

    public function fetch($requestId, $brokerId){

        return $this->recommendationService
            ->fetch($requestId, $brokerId);
    }

    public function fetchById($recommendationId){

        return $this->recommendationService
            ->fetchById($recommendationId);
    }

    public function choose(RecommendationChoose $request){

        $this->recommendationService
            ->chooseSlot(
                $request->input('recommendationId'),
                $request->input('slot'));
    }
}
