<?php

namespace App\Http\Controllers;

use App\Http\Requests\Reviews\ReviewRequest;
use App\Services\Applications\ApplicationService;
use App\Services\Reviews\ReviewService;

class ReviewController extends Controller
{

    protected ReviewService $reviewService;
    protected ApplicationService $applicationService;

    public function __construct(
        ApplicationService $applicationService,
        ReviewService $reviewService)
    {

        $this->applicationService = $applicationService;
        $this->reviewService = $reviewService;
    }

    public function review(ReviewRequest $request){

        $this->applicationService
            ->reviewable($request->input('request_id'));

        $this->reviewService
            ->review(
                $request->input('broker_id'),
                $request->input('rate'),
                $request->input('comment'));
    }

    public function list($userId){

        return $this->reviewService
            ->list(
                $userId);
    }
}
