<?php

namespace App\Http\Controllers;

use App\Http\Requests\Verification\GenerateRequest;
use App\Http\Requests\Verifications\VerifyRequest;
use App\Services\Notifications\Drivers\SmsDriver;
use App\Services\Verification\Enums\VerifyThrough;
use App\Services\Verification\VerificationService;

class VerificationController extends Controller
{

    protected VerificationService $verificationService;
    protected SmsDriver $smsDriver;

    public function __construct(
        VerificationService $verificationService,
        SmsDriver $smsDriver)
    {

        $this->verificationService = $verificationService;
        $this->smsDriver = $smsDriver;
    }


    public function generate(){

        return $this->verificationService->generate(
            VerifyThrough::API);
    }

    public function verify(VerifyRequest $request){

        return $this->verificationService->verify(
            $request->input('identifier'),
            $request->input('token'),
            VerifyThrough::API);
    }
}
