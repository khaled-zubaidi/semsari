<?php

namespace App\Models\Applications;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Request extends Model
{
    use HasFactory;

    protected $table = 'requests';
    protected $fillable = [
        'requester_id',
        'transaction_type',
        'property_type',
        'rent_from',
        'rent_to',
        'location',
        'price_from',
        'price_to',
        'specification',
        'area',
        'members',
        'special_request',
        'status'
    ];
}
