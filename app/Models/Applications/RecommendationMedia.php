<?php

namespace App\Models\Applications;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RecommendationMedia extends Model
{
    use HasFactory;

    protected $table = 'recommendation_media';
    protected $fillable = [
        'recommendation_id',
        'content',
        'type',
    ];
}
