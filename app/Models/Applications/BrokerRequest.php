<?php

namespace App\Models\Applications;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BrokerRequest extends Model
{
    use HasFactory;

    protected $table = 'broker_requests';
    protected $fillable = [
      'broker_id',
      'request_id',
      'status'
    ];
}
