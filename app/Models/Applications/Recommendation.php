<?php

namespace App\Models\Applications;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Recommendation extends Model
{
    use HasFactory;

    protected $table = 'recommendations';
    protected $fillable = [
        'requester_id',
        'broker_id',
        'transaction_type',
        'property_type',
        'location',
        'price',
        'specification',
        'area',
        'members',
        'status',
        'chosen_slot',
        'availability'
    ];
}
