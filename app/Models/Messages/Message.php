<?php

namespace App\Models\Messages;

use App\Models\Mailbox;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    use HasFactory;

    protected $table = 'messages';
    protected $fillable = [
        'mailbox_id',
        'sender_id',
        'message'
    ];

    public function getMailbox()
    {
        return $this->hasOne(Mailbox::class, 'mailbox_id');
    }

    public function getSender()
    {
        return $this->hasOne(User::class, 'sender_id');
    }
}
