<?php

namespace App\Models\Messages;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mailbox extends Model
{
    use HasFactory;

    protected $table = 'mailboxes';
    protected $fillable = [
        'user_id',
        'recipient_id'
    ];

    public function getRecipient()
    {
        return $this->hasOne(User::class, 'recipient_id');
    }

    public function getMailboxOwner()
    {
        return $this->hasOne(User::class, 'user_id');
    }
}
