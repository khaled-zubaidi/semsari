<?php

namespace App\Models\Profiles;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    use HasFactory;

    protected $table = 'profiles';
    protected $fillable = [
        'user_id',
        'gender',
        'location',
        'years_of_exp',
        'bio',
        'property_type',
        'transaction_type',
        'service_fees',
        'commission_rate',
    ];

    public function getUser()
    {
        return $this->hasOne(User::class, 'user_id');
    }
}
