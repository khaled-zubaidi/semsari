<?php

namespace App\Models\Verifications;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Verification\Verification
 *
 * @method static \Illuminate\Database\Eloquent\Builder|Verification newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Verification newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Verification query()
 * @mixin \Eloquent
 */
class Verification extends Model
{
    use HasFactory;

    protected $table = 'verifications';
    protected $fillable = [
        'identifier',
        'token',
        'is_used',
        'verify_through'
    ];
}
