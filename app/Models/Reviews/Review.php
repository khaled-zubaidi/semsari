<?php

namespace App\Models\Reviews;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    use HasFactory;

    protected $table = 'reviews';
    protected $fillable = [
        'comment',
        'rate',
        'rated_user',
        'rated_by',
    ];

    public function getRatedUser()
    {
        return $this->hasOne(User::class, 'rated_user');
    }

    public function getRatedBy()
    {
        return $this->hasOne(User::class, 'rated_by');
    }
}
