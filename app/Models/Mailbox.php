<?php

namespace App\Models;

use App\Models\Messages\Message;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mailbox extends Model
{
    use HasFactory;

    protected $table = 'mailboxes';
    protected $fillable = [
        'user_id',
        'recipient_id',
    ];

    public function getMessages(){

        return $this->hasMany(Message::class)->orderByDesc('created_at');
    }
}
