<?php

use App\Http\Controllers\ApplicationController;
use App\Http\Controllers\AuthenticationController;
use App\Http\Controllers\MessageController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\RecommendationController;
use App\Http\Controllers\RegistrationController;
use App\Http\Controllers\ReviewController;
use App\Http\Controllers\VerificationController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::controller(VerificationController::class)
    ->middleware('auth:sanctum')
    ->prefix('verification')
    ->group(function (){

    Route::get('/generate', 'generate');
    Route::post('/verify', 'verify');
});

Route::controller(MessageController::class)
    ->middleware('auth:sanctum')
    ->prefix('messages')
    ->group(function (){

        Route::get('/', 'list');
        Route::get('/fetch/{mailboxId}', 'fetch');
        Route::post('/', 'send');
    });

Route::controller(RegistrationController::class)
    ->prefix('registration')
    ->group(function (){

        Route::post('/register', 'register');
    });

Route::controller(AuthenticationController::class)
    ->prefix('auth')
    ->group(function (){

        Route::get('/logout', 'logout');
        Route::post('/login', 'login');
    });

Route::controller(ReviewController::class)
    ->middleware('auth:sanctum')
    ->prefix('reviews')
    ->group(function (){

        Route::get('/{userId}', 'list');
        Route::post('/', 'review');
    });

Route::controller(ProfileController::class)
    ->middleware('auth:sanctum')
    ->prefix('profiles')
    ->group(function (){

        Route::get('/fetch', 'fetch');
        Route::get('/fetchById/{brokerId}', 'fetchById');
        Route::post('/search', 'search');
        Route::post('/', 'create');
        Route::patch('/', 'update');
    });

Route::controller(ApplicationController::class)
    ->middleware('auth:sanctum')
    ->prefix('applications')
    ->group(function (){

        Route::post('/', 'create');
        Route::get('/', 'fetch');
        Route::get('/assigned', 'assigned');
        Route::patch('/reject/{requestId}', 'reject');
        Route::patch('/approve/{requestId}', 'approve');
        Route::patch('/close/{requestId}', 'close');
        Route::get('/fetchById/{requestId}', 'fetchById');
        Route::get('/broekrsByRequestId/{requestId}', 'broekrsByRequestId');

    });

Route::controller(RecommendationController::class)
    ->middleware('auth:sanctum')
    ->prefix('recommendations')
    ->group(function (){

        Route::post('/', 'create');
        Route::patch('/', 'choose');
        Route::get('/fetchById/{recommendationId}', 'fetchById');
        Route::get('/fetch/{requestId}/{brokerId}', 'fetch');
    });
